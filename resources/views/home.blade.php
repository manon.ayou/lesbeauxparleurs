@extends('layouts.app')

@section('content')
<p>ceci est la page d'accueil</p>

<form action="method-get.php" method="get">
  <div class="form-group">
    <label for="formGroupExampleInput">Envie de proposer un sujet ? </label>
    <input type="text" class="form-control" id="formGroupExampleInput" name="submit" placeholder="Tapez votre sujet ici">
  </div>
  <button type="button" class="btn btn-lg btn-primary" >Envoyer votre sujet</button>
</form>

<?php

   if ( isset( $_GET['submit'] ) ) {
     $submit = $_GET['submit'];
     echo '<h3>Informations récupérées en utilisant GET</h3>';
     echo '$submit';
     exit;
  }
?>

@endsection