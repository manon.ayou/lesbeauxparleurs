<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
}
)->name('redirecting');

Route::fallback(function() {
    return view('404'); // la vue
 });

Route::get('/home', function () {
    return view('home');
})->name('home');
Auth::routes();


Route::get('random',function() {
    return view('random');
});

Route::get('/adherents', function () {
    $users = App\users::all();

    return view('adherents', [
        'adherents' => $users
    ]);
});